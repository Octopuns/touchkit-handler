﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void MovePlatform(TKPanRecognizer r)
    {

        transform.position += new Vector3(r.deltaTranslation.x, r.deltaTranslation.y, 0.0f) * Time.deltaTime * 0.5f;

    }
}
