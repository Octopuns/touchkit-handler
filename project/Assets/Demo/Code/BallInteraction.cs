﻿//Demonstrates usage of the TouchKitInputHandler
using UnityEngine;
using System.Collections;

public class BallInteraction : MonoBehaviour {

 
    public Color deselectedColor;
    public Color selectedColor;

    private Rigidbody rigidbody;
    private MeshRenderer renderer;
    private bool selected = false;

    // Use this for initialization
    void Start () {

        //register for events via code instead of through the UnityEditor.
        TouchKitInputHandler tkInput = GameObject.FindGameObjectWithTag("GameController").GetComponent<TouchKitInputHandler>();

        tkInput.OnSwipe.AddListener(SwipeBall);
        tkInput.OnTap.AddListener(TapScreen);
        tkInput.OnPinch.AddListener(ScaleBall);

        renderer = GetComponent<MeshRenderer>();
        rigidbody = GetComponent<Rigidbody>();
        selected = true;
	}
	
	// Update is called once per frame
	void Update () {

        renderer.material.color = (selected?selectedColor : deselectedColor);

    }

    public void TapScreen(TKTapRecognizer r)
    {
        RaycastHit hit = new RaycastHit();
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1.0f));

        bool tappedBall = false;
        if (Physics.Raycast(ray, out hit, 100.0f))
        {
            if (hit.transform == transform)
            {
                selected = !selected;
                tappedBall = true;
         }          
        }

        if (tappedBall == false && selected)
        {
            rigidbody.AddForce(Vector3.up * 10.0f, ForceMode.Impulse);
        }
        
    }

    public void SwipeBall(TKAngleSwipeRecognizer r)
    {
        if (!selected)
            return;



#if UNITY_EDITOR || !UNITY_ANDROID
        rigidbody.AddForce(r.swipeVelocity * Vector3.forward * 25.0f, ForceMode.Impulse);
        rigidbody.AddForce(Vector3.up * r.swipeVelVector.y * 0.7f, ForceMode.Impulse);
#else
          rigidbody.AddForce(r.swipeVelocity * Vector3.forward * 40.0f, ForceMode.Impulse);
        rigidbody.AddForce(Vector3.up * r.swipeVelVector.y * 0.19f, ForceMode.Impulse);
#endif
        rigidbody.AddForce(Vector3.right * r.swipeVelVector.normalized.x * 20.0f, ForceMode.Impulse);
        selected = false;
    }

    public void ScaleBall(TKPinchRecognizer r)
    {
        if (!selected)
            return;

        if (r.deltaScale > 0.0f)
        {
            transform.localScale += transform.localScale * Time.deltaTime * 2.0f;
        }
        else
        {
            transform.localScale -= transform.localScale * Time.deltaTime * 2.0f;
        }
    }

}
