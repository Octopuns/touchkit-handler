﻿using UnityEngine;
using System.Collections;

public class BallSpawner : MonoBehaviour {

    public GameObject ballPrefab;

	

   public void SpawnBall(TKLongPressRecognizer r)
    {
        Instantiate(ballPrefab, transform.position, Quaternion.identity);
    }
}
