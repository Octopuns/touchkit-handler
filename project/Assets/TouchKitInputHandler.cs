﻿// Example Input Handler using the TouchKit Unity system: https://github.com/prime31/TouchKit
// This will handle differentiating swipes/taps/pinches both universally and location based.

using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class TouchKitInputHandler : MonoBehaviour {

    /// <summary>
    /// Events
    /// </summary>
    public TapTouchEvent OnTap;
    public PinchTouchEvent OnPinch;
    public PinchTouchEvent OnPinchComplete;
    public AngleSwipeTouchEvent OnSwipe;
    public LongPressTouchEvent OnLongPress;
    public PanTouchEvent OnPan;
    public PanTouchEvent OnPanComplete;

    private enum InputState
    {
        NONE,
        TAPPING,
        PINCHING,
        SWIPING,
        PANNING
    }

    private InputState currentPendingInput = InputState.NONE;


    /// <summary>
    /// TouchKit Gesture Settings
    /// </summary>
    /// 
    //minimum distance before a pinch is recognised
    private float pinchScaleDistance = 1.0f;
    
    //minimum distance before a swipe is recognised
    private float minSwipeDistance = 0.3f;
    
    //duration in seconds for it to be considered a tap
    private float maxTapDuration = 0.25f;
    
    //how much can the finger move during a tap
    private float maxFingerTapMovement = 0.25f;

    /// how much movement before it's recognised as a pan
    private float minPanDistance = 0.3f;


    void Awake()
    {
        // REGISTER PINCH
        var pinchRecognizer = new TKPinchRecognizer();
        pinchRecognizer.gestureRecognizedEvent += HandlePinch;
        pinchRecognizer.gestureCompleteEvent += EndPinch;
        pinchRecognizer.minimumScaleDistanceToRecognize = pinchScaleDistance;
        TouchKit.addGestureRecognizer(pinchRecognizer);

        // REGISTER SWIPE
        var swipeRecognizer = new TKAngleSwipeRecognizer();
        swipeRecognizer.gestureRecognizedEvent += HandleSwipe;
        swipeRecognizer.minimumDistance = minSwipeDistance;
        TouchKit.addGestureRecognizer(swipeRecognizer);

        // REGISTER TAP
        var tapRecognizer = new TKTapRecognizer(maxTapDuration, maxFingerTapMovement);
        tapRecognizer.gestureRecognizedEvent += HandleTap;
        TouchKit.addGestureRecognizer(tapRecognizer);

        // REGISTER LONGPRESS
        var longPressRecognizer = new TKLongPressRecognizer();
        longPressRecognizer.gestureRecognizedEvent += HandleLongPress;
        TouchKit.addGestureRecognizer(longPressRecognizer);

        //REGISTER PAN
        var panRecognizer = new TKPanRecognizer(minPanDistance);
        panRecognizer.minimumNumberOfTouches = 2;
        panRecognizer.maximumNumberOfTouches = 3;
        panRecognizer.gestureRecognizedEvent += HandlePan;
        panRecognizer.gestureCompleteEvent += CompletePan;
        TouchKit.addGestureRecognizer(panRecognizer);
    }







      ////////////////////////////////////
     // PAN                            //
    ////////////////////////////////////

    /// <summary>
    /// Event called during pan.
    /// </summary>
    /// <param name="r"></param>
    private void HandlePan(TKPanRecognizer r)
    {
        if (currentPendingInput == InputState.PINCHING)
            return;

        OnPan.Invoke(r);
        currentPendingInput = InputState.PANNING;
    }

    /// <summary>
    /// Event called on Pan completion.
    /// </summary>
    /// <param name="r"></param>
    private void CompletePan(TKPanRecognizer r)
    {
        if (currentPendingInput == InputState.PINCHING)
            return;

        OnPanComplete.Invoke(r);
        currentPendingInput = InputState.NONE;
    }









      ////////////////////////////////////
     // TAP                            //
    ////////////////////////////////////

    /// <summary>
    /// Event called on tap completion.
    /// </summary>
    /// <param name="r"></param>
    private void HandleTap(TKTapRecognizer r)
    {
        if (currentPendingInput == InputState.PINCHING || currentPendingInput == InputState.PANNING)
            return;

        if (r.state == TKGestureRecognizerState.Recognized)
            OnTap.Invoke(r);
    }

    public void HandleSwipe(TKAngleSwipeRecognizer r)
    {
        if (Input.touchCount > 1)
            return;

        if (currentPendingInput == InputState.PINCHING || currentPendingInput == InputState.PANNING)
            return;

        OnSwipe.Invoke(r);
    }








      ////////////////////////////////////
     // PINCH                          //
    ////////////////////////////////////

    /// <summary>
    /// Event called on Pinch update.
    /// </summary>
    /// <param name="r"></param>
    public void HandlePinch(TKPinchRecognizer r)
    {
        if (currentPendingInput == InputState.PANNING)
            return;

        OnPinch.Invoke(r);
        currentPendingInput = InputState.PINCHING;
    }

    /// <summary>
    /// Event called on Pinch completion.
    /// </summary>
    /// <param name="r"></param>
    public void EndPinch(TKPinchRecognizer r)
    {
        if (currentPendingInput == InputState.PANNING)
            return;

        OnPinchComplete.Invoke(r);
        currentPendingInput = InputState.NONE;
    }






      ////////////////////////////////////
     // LONG PRESS                     //
    ////////////////////////////////////

    /// <summary>
    /// Event called on LongPress completion.
    /// </summary>
    /// <param name="r"></param>
    public void HandleLongPress(TKLongPressRecognizer r)
    {
        if (Input.touchCount > 1)
            return;

        if (currentPendingInput == InputState.PINCHING || currentPendingInput == InputState.PANNING)
            return;
        OnLongPress.Invoke(r);
    }
   
}

//UnityEvent subclasses for editor-exposed events.
[System.Serializable]
public class TapTouchEvent : UnityEvent<TKTapRecognizer> { };

[System.Serializable]
public class PinchTouchEvent : UnityEvent<TKPinchRecognizer> { };

[System.Serializable]
public class AngleSwipeTouchEvent : UnityEvent<TKAngleSwipeRecognizer> { };

[System.Serializable]
public class LongPressTouchEvent : UnityEvent<TKLongPressRecognizer> { };

[System.Serializable]
public class PanTouchEvent : UnityEvent<TKPanRecognizer> { };